# Experimenting with the Git Diff Command

This repository provides a simple sandbox for experimenting with different forms of
the Git diff command.

To use it, clone the repository and then follow the instructions to set up different
versions of this README file, all labelled with information about how Git sees each
version and in particular how the Git diff command should be used to view each version.

When we use Git Diff, we want to see the difference in the contents of files in a
variety of states.  At any one time, a file in a Git repository exists in as many
versions as there are commits that made changes to it, plus two extra versions:
the file contents as they currently exist in the working directory and the file
contents as they were when the file was last staged for commit.  These last two
versions of the file can be the same as any of the versions in any of the commits
or they can be completely new versions of the file.


For example, focussing on just a single file, we might want to see:

* The differences between the file content in two consecutive commits ("what difference did
this particular commit make?").
* The difference between the file as it currently exists in our working directory and the
file as it was last committed to version control ("what changes have I made since the last
commit?").
* The difference between the file as it currently exists in our working directory and the
file as it was when we last staged it for commit ("what changes have I made since I last
staged this file for commit?").
* The difference between the version of the file currently staged for commit, and the
version committed in a particular commit ("What changes will my next commit make to this
file, if I commit it now?")

The Git diff command can be used to answer all these questions, but it can be tricky to
know exactly how to use the arguments to the command to get the kind of diff we want.

This repository gives a way to explore the different forms of the Git diff command, and
to develop familiarity with the forms you use more frequently in your own workflow.

Consult the official [documentation for the Git diff command](https://git-scm.com/docs/git-diff)
for more information and additional use cases.



## Instructions for Use

The repository contains three commits on the `main` branch, which each differ only in the
last line of this file.  This gives us examples of different committed versions of the
file to use in experimenting with the Git diff command.  We've tagged each commit
with the following tags, to make them easy to refer to:

* commitA
* commitB
* commitC

so be sure to fetch down all the tags when you clone.  You can check that the tags
are present with the following command and should see the result given:

    $ git tag
    commitA
    commitB
    commitC

If you don't see all these tags, you can ask Git to fetch them from this remote
repository using the following command:

    $ git fetch --all --tags


To fully explore the diff command, you also need to make the two additional versions in
your own local clone.  First, you should create a version of this file that is staged.

Edit this README.md file and change the last line to say that "This version is
staged for commit".

The stage this version with the command:

    $ git add README.md

Finally, you should make a change to the version of the file in your working directory.
So edit the file again and change the last line to say that "This version is changed in
the working directory".



## Using Git Diff

You can now try out these different versions of the git diff command in the Git clone you
have set up as described above.  See if you can work out from their outputs which versions
of the README file are compared with each command.

### Example 1

    $ git diff HEAD

or, to limit the diff to a specific file when several may have been changed:

    $ git diff HEAD -- README.md

### Example 2

    $ git diff

or, to limit the diff to a specific file when several may have been changed:

    $ git diff README.md

or

    $ git diff -- README.md

### Example 3

    $ git diff --staged


or, to limit the diff to a specific file when several may have been changed:

    $ git diff --staged README.md

or

    $ git diff --staged -- README.md

### Example 4

    $ git diff --staged commitB

or, to limit the diff to a specific file when several may have been changed:

    $ git diff --staged commitB -- README.md

### Example 5

    $ git diff commitB

or, to limit the diff to a specific file when several may have been changed:

    $ git diff commitB -- README.md

### Example 6

    $ git diff commitA commitC

or, to limit the diff to a specific file when several may have been changed:

    $ git diff commitA commitC -- README.md

### Example 7

    $ git diff commitC commitA

or, to limit the diff to a specific file when several may have been changed:

    $ git diff commitC commitA -- README.md




## Solutions


The diff command variants are:

* Example 1: compare the working directory contents with the most recent commit on the currently checked out branch.
* Example 2: compare the working directory contents with the currently staged changes
* Example 3: compare the currently staged changes with the most recent commit on the currently checked out branch
* Example 4: compare the currently staged changes with the contents of a named commit
* Example 5: compare the working directory contents with the contents of named commit
* Example 6: compare the contents of two named but not necessarily contiguous commits
* Example 7: compare the contents of two named commits in the opposite order

We have omitted the following main forms of the command:

* Comparison of the contents of two files in the current file system, either or both of which may be in a Git working directory
* Comparison of the contents of two arbitrary Git blobs
* Comparison of two named commits relative to a given merge base
* Comparison of commits making up a merge or commits in a range of commit (using the '..' and '...' notation)


____

This is the version at commit C.

